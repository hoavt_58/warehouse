package controllers;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Ebean;

import models.Product;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.*;

public class Products extends Controller{
	private static final Form<Product> productForm = Form.form(Product.class);
	// List all products
	public static Result list(){
		List<Product> products = Product.findAll();
	    return ok(list.render(products));
	}
	// Show a blank product form
	public static Result newProduct(){
		return ok(details.render(productForm));
	}
	// Show a product edit form
	public static Result details(Product product) {
        if (product == null) {
            return notFound(String.format("Product %s does not exist.",
                            product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }
	// Save a product
	public static Result save(){
		Form<Product> boundForm = productForm.bindFromRequest();
		if (boundForm.hasErrors()) {
		    flash("error", "Please correct the form below.");
		    return badRequest(details.render(boundForm));
		}
		Product product = boundForm.get();
		List<Tag> tags = new ArrayList<Tag>();
	    for (Tag tag : product.tags) {
	      if (tag.id != null) {
	        tags.add(Tag.findById(tag.id));
	      }
	    }
	    product.tags = tags;
	    //product.save();
	    Ebean.save(product);
	    flash("success", String.format("Successfully added product %s", product));
	    return redirect(routes.Products.list());
	}

	public static Result delete(Product product) {
		  if(product == null) {
		    return notFound(String.format("Product %s does not exists.", product.ean));
		  }
		  //Product.remove(product);
		  product.delete();
		  return redirect(routes.Products.list());
		}
}
